/*
 * lazy_list_iterator.h
 *
 *  Created on: 2 февр. 2016 г.
 *      Author: hokum
 */

#ifndef LAZY_LIST_ITERATOR_H_
#define LAZY_LIST_ITERATOR_H_

#include <iterator>

namespace __details_lazy_list
{

template< typename lazy_list_type >
class const_iterator:
    public std::iterator<std::input_iterator_tag, typename lazy_list_type::value_type>
{
    friend lazy_list_type;
public:
    typedef std::iterator<std::input_iterator_tag, typename lazy_list_type::value_type> base_type;
    const_iterator();

    typename base_type::reference const operator* () const;
    typename base_type::pointer const operator-> () const;

    const_iterator& operator++();
    const_iterator operator++(int);

    bool operator== (const const_iterator& that);
    bool operator!= (const const_iterator& that);
private:
    typedef typename lazy_list_type::inner_iterator inner_iterator;

    const_iterator(const lazy_list_type* owner, inner_iterator iter);

    const lazy_list_type* _owner;
    inner_iterator _iter;
};

template<typename lazy_list_type>
inline const_iterator<lazy_list_type>::const_iterator () :
    _owner(nullptr)
{
}

template<typename lazy_list_type>
inline const_iterator<lazy_list_type>::const_iterator (
    const lazy_list_type* owner,
    inner_iterator iter
    ) :
    _owner(owner),
    _iter(iter)
{
}

template<typename lazy_list_type>
typename const_iterator<lazy_list_type>::base_type::reference const const_iterator<lazy_list_type>::operator* () const
{
    return *_iter;
}

template<typename lazy_list_type>
typename const_iterator<lazy_list_type>::base_type::pointer const const_iterator<lazy_list_type>::operator-> () const
{
    return _iter.operator->();
}

template<typename lazy_list_type>
inline const_iterator<lazy_list_type>& const_iterator<lazy_list_type>::operator ++ ()
{
    if (_owner)
        _owner->forward(*this);
    return *this;
}

template<typename lazy_list_type>
inline const_iterator<lazy_list_type> const_iterator<lazy_list_type>::operator ++ (int)
{
    const_iterator iter(*this);
    ++(*this);
    return iter;
}


template<typename lazy_list_type>
inline bool const_iterator<lazy_list_type>::operator == (const const_iterator& that)
{
    return (_owner == that._owner) && (_iter == that._iter);
}

template<typename lazy_list_type>
inline bool const_iterator<lazy_list_type>::operator != (const const_iterator& that)
{
    return !(*this == that);
}

} //__details_lazy_list

#endif /* LAZY_LIST_ITERATOR_H_ */
