/*
 * lazy_list.h
 *
 *  Created on: 1 февр. 2016 г.
 *      Author: hokum
 */

#ifndef LAZY_LIST_H_
#define LAZY_LIST_H_

#include "lazy_list_iterator.h"

#include <list>
#include <map>
#include <utility>
#include <functional>
#include <cassert>

namespace lazy
{

template< typename T, typename Allocator = std::allocator<T> >
class list
{
public:
    typedef list<T, Allocator> self_type;
    typedef T value_type;
    typedef std::function<self_type ()> func_type;
    typedef __details_lazy_list::const_iterator<self_type> iterator;
    typedef __details_lazy_list::const_iterator<self_type> const_iterator;

    friend __details_lazy_list::const_iterator<self_type>;

    list();
    list(const self_type& that);
    list(self_type&& that);

    template<typename ... Args>
    explicit list(Args&&... args)
    {
        push_others(std::forward<Args>(args)...);
    }

    void push_back(const value_type& value);
    void push_back(value_type&& value);
    void push_back(const func_type& func);
    void push_back(func_type&& func);
    void push_back(const self_type& that);
    void push_back(self_type&& that);

    void push_front(const value_type& value);
    void push_front(value_type&& value);
    void push_front(const func_type& func);
    void push_front(func_type&& func);
    void push_front(const self_type& that);
    void push_front(self_type&& that);


    void clear();

    bool empty() const;

    const_iterator begin() const;
    const_iterator end() const;

private:
    typedef std::list<value_type, Allocator> container_type;
    typedef typename container_type::iterator inner_iterator;
    typedef value_type const * funcs_map_key_type;
    typedef std::pair<funcs_map_key_type, func_type> funcs_map_value_type;
    typedef std::map<funcs_map_key_type, func_type> funcs_map_type;

    void forward(const_iterator& iter) const;
    void insert(inner_iterator pos, self_type&& that) const;

    template<typename Arg, typename ...Args>
    void push_others(Arg&& arg, Args&&... args)
    {
        push_back(std::forward<Arg>(arg));
        push_others(std::forward<Args>(args)...);
    }

    template<typename Arg>
    void push_others(Arg&& arg)
    {
        push_back(std::forward<Arg>(arg));
    }

    void push_others() {}

    mutable container_type _cont;
    mutable funcs_map_type _funcs;
};

template< typename T, typename Allocator>
inline list<T, Allocator>::list()
{}

template< typename T, typename Allocator>
inline list<T, Allocator>::list(const self_type& that)
{
    push_back(that);
}

template< typename T, typename Allocator>
inline list<T, Allocator>::list(self_type&& that) :
    _cont(std::move(that._cont)),
    _funcs(std::move(that._funcs))
{
}

template< typename T, typename Allocator>
inline bool list<T, Allocator>::empty() const
{
    return _cont.empty();
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_back(const value_type& value)
{
    _cont.push_back(value);
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_back(value_type&& value)
{
    _cont.emplace_back(std::move(value));
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_back(const func_type& func)
{
    if (_cont.empty())
    {
        push_back(func());
    }
    else
    {
        auto last = --_cont.end();
        auto func_it = _funcs.find(last.operator->());
        if (func_it == _funcs.end())
            _funcs.insert(funcs_map_value_type(
                    last.operator->(),
                    func
                ));
        else
            push_back(func());
    }
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_back(func_type&& func)
{
    if (_cont.empty())
    {
        push_back(func());
    }
    else
    {
        auto last = --_cont.end();
        auto func_it = _funcs.find(last.operator->());
        if (func_it == _funcs.end())
            _funcs.insert(funcs_map_value_type(
                    last.operator->(),
                    std::move(func)
                ));
        else
            push_back(func());
    }
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::push_back(const self_type& that)
{
    assert(&that != this);
    for(auto it = that._cont.begin(); it != that._cont.end(); ++it)
    {
        auto func_it = that._funcs.find(it.operator->());
        _cont.push_back(*it);
        if ((func_it != that._funcs.end()) && (func_it->second))
        {
            _funcs.insert(funcs_map_value_type(
                    (--_cont.end()).operator->(),
                    func_it->second
                ));
        }
    }
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::push_back(self_type&& that)
{
    assert(&that != this);
    for(auto it = that._cont.begin(); it != that._cont.end(); ++it)
    {
        auto func_it = that._funcs.find(it.operator->());
        _cont.push_back(std::move(*it));
        if ((func_it != that._funcs.end()) && (func_it->second))
        {
            _funcs.insert(funcs_map_value_type(
                    (--_cont.end()).operator->(),
                    std::move(func_it->second)
                ));
        }
    }
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_front(const value_type& value)
{
    _cont.push_front(value);
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_front(value_type&& value)
{
    _cont.emplace_front(std::move(value));
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_front(const func_type& func)
{
    push_front(func());
}

template<typename T, typename Allocator >
inline void list<T, Allocator>::push_front(func_type&& func)
{
    push_front(func());
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::push_front(const self_type& that)
{
    assert(&that != this);
    auto place = _cont.begin();
    for(auto it = that._cont.begin(); it != that._cont.end(); ++it)
    {
        auto func_it = that._funcs.find(it.operator->());
        auto inserted = _cont.insert(place, *it);
        if ((func_it != that._funcs.end()) && (func_it->second))
        {
            _funcs.insert(funcs_map_value_type(
                    inserted.operator->(),
                    func_it->second
                ));
        }
    }
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::push_front(self_type&& that)
{
    insert(_cont.begin(), std::move(that));
}


template<typename T, typename Allocator>
inline void list<T, Allocator>::clear()
{
    _cont.clear();
    _funcs.clear();
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::insert(inner_iterator pos, self_type&& that) const
{
    assert(&that != this);
    for(auto it = that._cont.begin(); it != that._cont.end(); ++it)
    {
        auto func_it = that._funcs.find(it.operator->());
        auto inserted = _cont.emplace(pos, std::move(*it));

        if ((func_it != that._funcs.end()) && (func_it->second))
        {
            _funcs.insert(funcs_map_value_type(
                    inserted.operator->(),
                    std::move(func_it->second)
                ));
        }
    }
}

template<typename T, typename Allocator>
inline typename list<T, Allocator>::const_iterator list<T, Allocator>::begin() const
{
    return const_iterator(this, _cont.begin());
}

template<typename T, typename Allocator>
inline typename list<T, Allocator>::const_iterator list<T, Allocator>::end()  const
{
    return const_iterator(this, _cont.end());
}

template<typename T, typename Allocator>
inline void list<T, Allocator>::forward(const_iterator& iter) const
{
    if (iter._iter == _cont.end())
        return;

    auto func_it = _funcs.find(iter._iter.operator->());
    if (func_it != _funcs.end())
    {
        auto pos_insert = iter._iter;
        ++pos_insert;
        insert(pos_insert, (func_it->second)());
        _funcs.erase(func_it);
    }
    ++iter._iter;
}

}//namespace lazy

#endif /* LAZY_LIST_H_ */
