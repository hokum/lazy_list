CXXFLAGS =	-ggdb -std=c++14 -g -Wall -fmessage-length=0 -I..

OBJS =		tests/tests_lazy_list.o

LIBS =

OUTPUT_DIR = bin

TARGET =	tests_lazy_list


.PHONY: all clean

ifndef parentdir

DIRS = $(dir $(OBJS))

export parentdir=..

all:
	mkdir -p $(OUTPUT_DIR)
	mkdir -p $(addprefix $(OUTPUT_DIR)/,$(DIRS))
	$(MAKE) --directory=$(OUTPUT_DIR) --makefile=../Makefile

else

VPATH = $(parentdir)

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

tests/tests_lazy_list.o: tests/catch.hpp lazy_list.h lazy_list_iterator.h

all:	$(TARGET)

tests/catch.hpp:
	wget https://raw.githubusercontent.com/philsquared/Catch/master/single_include/catch.hpp -O $(parentdir)/tests/catch.hpp

endif

clean:
	rm -rf $(OUTPUT_DIR)

