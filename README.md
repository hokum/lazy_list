# README #

lazy_list это реализация списка элементы которого могут вычислятся при первом обращении к ним. В сам список могут добавлятся как элементы, так и функторы для генерации.

В качестве фреймворка для юнит тестов используется Catch (https://github.com/philsquared/Catch/). Для сборки тестов нужно скачать файл catch.hpp и положить его в подкаталог tests.

Ссылка для скачивания catch.hpp: https://raw.githubusercontent.com/philsquared/Catch/master/single_include/catch.hpp

В линуксе можно просто выполнить комманду make. Файл catch.hpp будет скачан автоматически.